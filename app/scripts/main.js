$(document).ready(function() {

  new Swiper('.oc-big', {
    navigation: {
      nextEl: '.oc-big .swiper-button-next',
      prevEl: '.oc-big .swiper-button-prev',
    },
    pagination: {
      el: '.oc-big .swiper-pagination',
    },
    breakpoints: {
      1290: {
        spaceBetween: 10,
        slidesPerView: 'auto',
        centeredSlides: true,
        loop: true,
        pagination: {
          el: null,
        }
      }
    }
  });

  new Swiper('.oc-news__cards', {
    slidesPerView: 'auto',
    spaceBetween: 20,
    navigation: {
      nextEl: '.oc-news .swiper-button-next',
      prevEl: '.oc-news .swiper-button-prev',
    },
    breakpoints: {
      1290: {
        spaceBetween: 10,
        slidesPerView: 'auto',
        centeredSlides: true,
        loop: true,
        pagination: {
          el: null,
        }
      }
    }
  });

  new Swiper('.oc-coupon__cards', {
    slidesPerView: 'auto',
    spaceBetween: 20,
    navigation: {
      nextEl: '.oc-coupon .swiper-button-next',
      prevEl: '.oc-coupon .swiper-button-prev',
    },
    breakpoints: {
      1290: {
        spaceBetween: 10,
        slidesPerView: 'auto',
        centeredSlides: true,
        loop: true,
        pagination: {
          el: null,
        }
      }
    }
  });

  new Swiper('.oc-gallery__cards', {
    pagination: {
      el: '.swiper-pagination',
      type: 'fraction',
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
  });

  new Swiper('.oc-seo__cards', {
    pagination: {
      el: '.swiper-pagination',
      type: 'fraction',
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
  });

  new Swiper('.oc-menu__cards', {
    pagination: {
      el: '.swiper-pagination',
      type: 'fraction',
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
  });

  $('.oc-header__toggle').on('click', function(e) {
    e.preventDefault();
    $('.oc-sidebar').toggleClass('oc-sidebar_show');
  });

  $('.oc-sidebar__close').on('click', function(e) {
    e.preventDefault();
    $('.oc-sidebar').toggleClass('oc-sidebar_show');
  });
});